node -v && npm -v && git --version
if %errorlevel% neq 0(
    echo ERROR: "node" and "npm" and "git" is required.
    exit /b
)

pushd %~dp0
call npm init
call npm install --save-dev @vue/cli
call node_modules\.bin\vue create .
call git add *
call git commit -m "Create pure Vue.js project."
call node_modules\.bin\vue add electron-builder
call git add *
call git commit -m "Add electron builder."
call npm run electron:serve
popd
