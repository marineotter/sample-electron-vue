# sample-electron-vue

## Project setup
```
npm install
```

## Start for electron

### Start
```
npm run electron:serve
```

### Build
```
npm run electron:build
```

## Start for web

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
