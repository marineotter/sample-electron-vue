module.exports = {
    pluginOptions: {
      electronBuilder: {
        builderOptions: {
          productName: "sample-electron-vue",
          appId: "org.kawauso-lab.sample-electron-vue",
          win: {
            // icon: 'src/assets/app.ico',
            target: [
              {
                target: 'nsis', // 'zip', 'nsis', 'portable'
                arch: ['x64'] // 'x64', 'ia32'
              }
            ]
          }
        }
      }
    }
  }